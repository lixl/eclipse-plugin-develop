package com.demo.swtdemo;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.TitleEvent;
import org.eclipse.swt.browser.TitleListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

/**
 * Hello world!
 * 
 */
public class App {
	public static void main1(String[] args) {
		System.out.println("Hello World!");
		Display display = new Display();

		// 获取屏幕宽高
		Monitor monitor = display.getPrimaryMonitor();
		System.out.println(monitor.getClientArea());// 客户区域
		System.out.println(monitor.getBounds());// 总边界

		Shell shell = new Shell(display);
		shell.setText("Demo Shell");
		shell.setSize(400, 200);

		shell.open();
		shell.layout();

		Browser browser = new Browser(shell, SWT.DEFAULT);
		browser.setUrl("http://www.baidu.com");

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	
	public static void main(String[] args) throws IOException, URISyntaxException {
		java.awt.Desktop dp = java.awt.Desktop.getDesktop();
		URI uri = new URI("http://www.baidu.com");
		dp.browse(uri);
	}

	public static void main3(String[] args) {

		Display display = new Display();
		final Shell shell = new Shell(display);
		FillLayout layout = new FillLayout();
		shell.setLayout(layout);

		Browser browser = new Browser(shell, SWT.DEFAULT); // 1
		browser.addTitleListener(new TitleListener() { // 2
			public void changed(TitleEvent event) {
				shell.setText(event.title);
			}
		});
		browser.setUrl("http://192.163.20.17:8080/20150509SSH_Study/index.jsp"); // 3
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
