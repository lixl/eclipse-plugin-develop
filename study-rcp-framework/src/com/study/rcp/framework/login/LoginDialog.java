package com.study.rcp.framework.login;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.wb.swt.SWTResourceManager;

import com.study.rcp.framework.core.Activator;

public class LoginDialog extends TitleAreaDialog {

	private Text passwordText;
	private Text userNameText;

	/**
	 * Create the dialog
	 * 
	 * @param parentShell
	 */
	public LoginDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(SWT.NO_TRIM);

	}

	/**
	 * Create contents of the dialog
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		// 采用三列的GridLayout

		GridLayout layout = new GridLayout(3, false);

		// 以下数值通过实验调整得出，这样看起来能够使输入框在中间位置，比较美观

		layout.marginHeight = 20;

		layout.marginWidth = 70;

		layout.verticalSpacing = 10;

		layout.horizontalSpacing = 10;

		container.setLayout(layout);

		// 注意充满FILL_BOTH

		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		container.setFont(parent.getFont());

		// final GridLayout gridLayout = new GridLayout();
		// gridLayout.numColumns = 2;
		// container.setLayout(gridLayout);
		// GridData gd_container = new GridData(GridData.FILL_BOTH);
		// gd_container.heightHint = 65;
		// container.setLayoutData(gd_container);
		//
		// final Label label = new Label(container, SWT.NONE);
		// label.setText("用户名:");
		// Font boldFont = JFaceResources.getFontRegistry().getBold(
		// JFaceResources.DEFAULT_FONT);
		// label.setFont(boldFont);
		// userNameText = new Text(container, SWT.BORDER);
		// userNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
		// false));
		//
		// final Label label_1 = new Label(container, SWT.NONE);
		// label_1.setText("密码:");
		// label_1.setFont(boldFont);
		// passwordText = new Text(container, SWT.BORDER);
		// passwordText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
		// false));
		// passwordText.setEchoChar('*');
		// Label user

		Label user = new Label(container, SWT.NULL);
		// 右对齐
		GridData layoutData = new GridData(GridData.HORIZONTAL_ALIGN_END);
		user.setLayoutData(layoutData);
		Font boldFont = JFaceResources.getFontRegistry().getBold(
				JFaceResources.DEFAULT_FONT);
		user.setText("用户名:");
		user.setFont(boldFont);
		// 设置提示信息
		// Text user
		userNameText = new Text(container, SWT.BORDER | SWT.LEAD);
		userNameText.setToolTipText("请输入用户名");
		// 横向扩充
		layoutData = new GridData(GridData.GRAB_HORIZONTAL

		| GridData.FILL_HORIZONTAL);
		// horizontalSpan=2 是让输入框占两个格的位置。这样看起来比较美观
		layoutData.horizontalSpan = 2;
		userNameText.setLayoutData(layoutData);
		// Label password
		Label password = new Label(container, SWT.NULL);
		layoutData = new GridData(GridData.HORIZONTAL_ALIGN_END);
		password.setLayoutData(layoutData);
		password.setText("密码:");
		password.setFont(boldFont);
		// Text password
		passwordText = new Text(container, SWT.BORDER | SWT.PASSWORD);
		passwordText.setToolTipText("请输入用户密码");
		layoutData = new GridData(GridData.GRAB_HORIZONTAL
		| GridData.FILL_HORIZONTAL);

		layoutData.horizontalSpan = 2;
		passwordText.setLayoutData(layoutData);
		passwordText.setEchoChar('*');
		setTitle("欢迎登录Java开发应用基础平台");
		setMessage("输入用户名和密码");
		//
		return area;
	}

	/**
	 * Create contents of the button bar
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button button = createButton(parent, IDialogConstants.CANCEL_ID, "取消",
				false);
		button.setText("退出");
		button.setImage(AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
				"icons/close.png").createImage());
		button.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 9,
				SWT.BOLD));
		Button button_1 = createButton(parent, IDialogConstants.OK_ID, "登陆",
				true);
		button_1.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 9,
				SWT.BOLD));
		button_1.setImage(AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
				"icons/user6.png").createImage());
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(354, 229);
	}

	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("欢迎登录");
		// 居中窗口
		Shell shell = newShell;
		setTitleImage(AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
				"icons/left-logo.jpg").createImage());
		shell.setBackgroundImage(AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
				"icons/default.jpg").createImage());
		Rectangle screenSize = Display.getDefault().getClientArea();
		Rectangle frameSize = shell.getBounds();
		shell.setLocation((screenSize.width - frameSize.width) / 2,
				(screenSize.height - frameSize.height)/2 );
//		System.out.println(screenSize.width);
//		System.out.println(frameSize.width);
//		System.out.println((screenSize.width - frameSize.width) / 2);
//		shell.setBounds((screenSize.width) / 2, (frameSize.height)/2, 354, 229);
		shell.setLocation((screenSize.width - frameSize.width) / 2,  
				(screenSize.height - frameSize.height) / 2); 
		shell.setBackgroundImage(AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
				"icons/left-logo.jpg").createImage());
	}

	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			try {
				login(userNameText.getText(), passwordText.getText());
				okPressed();
			} catch (Exception e) {
				MessageDialog.openError(getShell(), "登录失败", e.getMessage());
				e.printStackTrace();
				return;
			}
		}
		if (buttonId == IDialogConstants.CANCEL_ID) {
			cancelPressed();
		}
		super.buttonPressed(buttonId);
	}

	/**
	 * <p>
	 * 判断是否登录成功。
	 * </p>
	 * 
	 * @author 刘绕兴
	 * @param userName
	 * @param password
	 * @throws Exception
	 */
	private void login(String userName, String password) throws Exception {
		if (userName.equals("") && password.equals(""))
			return;
		if (userName.equals("zhujinfei") && password.equals("zhujinfei"))
			return;
		throw new Exception("错误的用户名和密码");
	}

}