/*
 * file name   : Welcome.java 
 * <br>copyright   : Copyright (c) 2015
 * <br>Company: 联动优势电子商务有限公司
 * <br>description : TODO<何时使用>[注意][示例]
 * <br>modified    : 
 * @author      Zhu Jinfei
 * @version     1.0
 * @date        2015年7月5日 上午12:15:06
 */ 
package com.study.rcp.framework.login;

import java.util.Random;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.study.rcp.framework.core.Activator;

/*************************************************************************
 * <br>description : TODO
 * @author      Zhu Jinfei
 * @version     1.0  
 * @date        2015年7月5日 上午12:15:06 
 * @see         TODO                 
 *************************************************************************/
public class Welcome extends ViewPart {

	public static final String ID = "login.Welcome"; //$NON-NLS-1$

	public Welcome() {
	}

	/**
	 * Create contents of the view part.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		int i=Math.abs(new Random().nextInt()%4);
		container.setBackgroundImage(AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
				"icons/"+i+".jpg").createImage());
		createActions();
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	@Override
	public void setFocus() {
		// Set the focus
	}

}
