package com.study.rcp.framework.core;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.splash.AbstractSplashHandler;

/**
 * The splash handler overrides the default RCP splash handler.
 */
public class LoginSplashHandler extends AbstractSplashHandler {

	
	private Composite loginComposite;
	private Text usernameTextBox;
	private Text passwordTextBox;
	private Button okButton;
	private Button cancelButton;
	private boolean isAuthenticated;
	private Label usernameLabel;
	private Label passwordLabel;

	public LoginSplashHandler() {
		isAuthenticated = false;
	}

	public void init(final Shell splash) {
		splash.setMinimumSize(new Point(400, 165));
		System.out.println("In splash windows.");
		super.init(splash);

		configureUISplash();
		createUI();
		createUIListeners();

		// 显示各界面元素。
		splash.layout(true);

		/**
		 * Create the event loop for the splash to prevent the application load
		 * from completion, and hold it at the splash until the login event is
		 * successful.
		 */
		while (isAuthenticated == false) {
			if (splash.getDisplay().readAndDispatch() == false) {
				splash.getDisplay().sleep();
			}
		}
	}

	/**
	 * Create the UI listeners for all the form components.
	 */
	private void createUIListeners() {
		// Create the OK button listeners.
		okButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleButtonOKWidgetSelected();
			}
		});

		// Create the cancel button listeners.
		cancelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				/**
				 * Abort the loading of the RCP application.
				 */
				getSplash().getDisplay().close();
				System.exit(0);
			}
		});
	}

	/**
	 * Handles the OK button being pressed and the login attempted.
	 */
	private void handleButtonOKWidgetSelected() {
		String username = usernameTextBox.getText();
		String password = passwordTextBox.getText();

		// AuthenticationClient client = new AuthenticationClient();

		if (username.equals("") || password.equals("")) {
			MessageDialog.openError(getSplash(), "Authentication Failed", //$NON-NLS-1$
					"A username and password must be specified to login."); //$NON-NLS-1$
		} else {
			// try {
			// if (client.authenticate(username, password)) {
			if (true) {
				isAuthenticated = true;
			} else {
				MessageDialog.openError(getSplash(), "Authentication Failed", //$NON-NLS-1$
						"The details you entered could not be verified."); //$NON-NLS-1$
			}
			// } catch (MalformedURLException e) {
			//				MessageDialog.openError(getSplash(), "Authentication Failed", //$NON-NLS-1$
			//						"Service responded with an error."); //$NON-NLS-1$
			// }
		}
	}

	/**
	 * Calls the individual UI component creation functions.
	 */
	private void createUI() {
		// Create the login panel.
		loginComposite = new Composite(getSplash(), SWT.BORDER);
		loginComposite.setLayout(null);

		// Create the user name label.
		usernameLabel = new Label(loginComposite, SWT.NONE);
		usernameLabel.setBounds(153, 50, 69, 17);
		usernameLabel.setText("&User Name:");

		// Create the user name text widget.
		usernameTextBox = new Text(loginComposite, SWT.BORDER);
		usernameTextBox.setBounds(228, 76, 141, 23);
		usernameTextBox.setText("xxx");

		// Create the password label.
		passwordLabel = new Label(loginComposite, SWT.NONE);
		passwordLabel.setBounds(163, 79, 59, 17);
		passwordLabel.setText("&Password:");

		// Create the password text widget.
		int style = SWT.PASSWORD | SWT.BORDER;
		passwordTextBox = new Text(loginComposite, style);
		passwordTextBox.setBounds(228, 47, 141, 23);
		passwordTextBox.setText("xxx");

		// Create the OK button.
		okButton = new Button(loginComposite, SWT.PUSH);
		okButton.setBounds(156, 118, 100, 30);
		okButton.setText("OK");

		// Create the cancel button.
		cancelButton = new Button(loginComposite, SWT.PUSH);
		cancelButton.setBounds(269, 118, 100, 30);
		cancelButton.setText("Cancel");
	}

	/**
	 * Configures the splash screen SWT/UI components.
	 */
	private void configureUISplash() {
		// Configure layout
		FillLayout layout = new FillLayout();
		getSplash().setLayout(layout);

		// Force shell to inherit the splash background
		getSplash().setBackgroundMode(SWT.INHERIT_DEFAULT);
	}
}