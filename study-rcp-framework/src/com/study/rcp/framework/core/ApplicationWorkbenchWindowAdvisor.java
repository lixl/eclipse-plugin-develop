package com.study.rcp.framework.core;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPreferenceConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

    public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
        super(configurer);
    }

    public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
        return new ApplicationActionBarAdvisor(configurer);
    }
    public void preWindowOpen() {
		IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
//		configurer.setInitialSize(new Point(200, 300));
		configurer.setShowFastViewBars(true);
		configurer.setShowPerspectiveBar(true);
		configurer.setShowProgressIndicator(true);
		configurer.setShowCoolBar(true);
		configurer.setShowStatusLine(true);
		IPreferenceStore preferenceStore = PlatformUI.getPreferenceStore();
		PlatformUI.getPreferenceStore().setDefault(
				IWorkbenchPreferenceConstants.ENABLE_ANIMATIONS, true);
		preferenceStore.setValue(
				IWorkbenchPreferenceConstants.SHOW_TRADITIONAL_STYLE_TABS,
				false);
		preferenceStore.setValue(
				IWorkbenchPreferenceConstants.DOCK_PERSPECTIVE_BAR,
				IWorkbenchPreferenceConstants.TOP_RIGHT);
		preferenceStore.setValue(
	                IWorkbenchPreferenceConstants.SHOW_INTRO, true);
		preferenceStore.setValue(
				IWorkbenchPreferenceConstants.SHOW_PROGRESS_ON_STARTUP, true);
		getWindowConfigurer().getWindow().getWorkbench().getIntroManager().showIntro(getWindowConfigurer().getWindow(),false);
	}

	@Override
	public void postWindowOpen() {
		// TODO Auto-generated method stub
		super.postWindowCreate();
		// 设置打开时最大化窗口
		getWindowConfigurer().getWindow().getShell().setMaximized(true);
		getWindowConfigurer().getWindow().getShell().setBackgroundImage(AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID,
				"icons/default.jpg").createImage());
		
	}

	@Override
	public boolean preWindowShellClose() {
		// TODO Auto-generated method stub
		 // TODO Auto-generated method stub   
	    MessageBox msgBox = new MessageBox(new Shell(), SWT.YES | SWT.NO | SWT.ICON_QUESTION);  
	    msgBox.setText("退出系统");  
	    msgBox.setMessage("确定退出Java开发应用基础平台?");  
	    if (msgBox.open() == SWT.YES) {  
	        return true;  
	    }  
	    return false;  

	}
    
}
