package com.study.rcp.framework.core;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

/**
 * This workbench advisor creates the window advisor, and specifies
 * the perspective id for the initial window.
 */
public class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {
	
    public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
        return new ApplicationWorkbenchWindowAdvisor(configurer);
    }
    TrayItemManager trayManager;
	public String getInitialWindowPerspectiveId() {
		return Perspective.ID;
	}

	@Override
	public void initialize(IWorkbenchConfigurer configurer) {
		// TODO Auto-generated method stub
		super.initialize(configurer);
		configurer.setSaveAndRestore(false);
	} 
	
	

    public void postStartup() {
        super.postStartup();
//        final Shell shell = PlatformUI.getWorkbench()
//                .getActiveWorkbenchWindow().getShell();
//        shell.addShellListener(new ShellAdapter() {
//            public void shellIconified(ShellEvent e) {
//                shell.setVisible(false);
//            }
//        });
//        createSystemTray();

    }

    private void createSystemTray() {
        Tray tray = Display.getDefault().getSystemTray();
        TrayItem item = new TrayItem(tray, SWT.NONE);
        item.setText("JDishNetwork");
        item.setToolTipText("JDishNetwork");
        Image image = new Image(Display.getDefault(), 16, 16);
        item.setImage(image);
        this.trayManager = new TrayItemManager();
        item.addSelectionListener(this.trayManager);
        item.addListener(SWT.MenuDetect, this.trayManager);
    }

    private class TrayItemManager implements SelectionListener, Listener {
        // TODO: Convert to one class
        private final class WindowStateListener extends SelectionAdapter {
            public void widgetSelected(SelectionEvent e) {
                minimizeWindow();
            }
        }

        private final class RestoreWindowListener extends SelectionAdapter {
            public void widgetSelected(SelectionEvent e) {
                restoreWindow();
            }
        }

        private boolean minimized = false;

        private Menu menu;

        private MenuItem[] menuItems = new MenuItem[0];

        private RestoreWindowListener restoreWindowListener;

        private WindowStateListener minimizeWindowListener;

        public TrayItemManager() {
            this.menu = new Menu(PlatformUI.getWorkbench()
                    .getActiveWorkbenchWindow().getShell(), SWT.POP_UP);
            this.restoreWindowListener = new RestoreWindowListener();
            this.minimizeWindowListener = new WindowStateListener();

        }

        protected void closeApplication() {
            PlatformUI.getWorkbench().close();
        }

        public void widgetSelected(SelectionEvent e) {
            //
        }

        public void widgetDefaultSelected(SelectionEvent e) {
            if (this.minimized) {
                restoreWindow();
            } else {
                minimizeWindow();
            }
        }

        protected void minimizeWindow() {
            PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell()
                    .setMinimized(true);
            this.minimized = true;
        }

        protected void restoreWindow() {
            Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
                    .getShell();
            shell.open();
            shell.setMinimized(false);
            shell.forceActive();
            shell.forceFocus();
            this.minimized = false;
        }

        public void showMenu() {
            clearItems();
            MenuItem mi;
            MenuItem closeItem;
            mi = new MenuItem(this.menu, SWT.PUSH);
            closeItem = new MenuItem(this.menu, SWT.NONE);
            closeItem.setText("Close");
            closeItem.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    closeApplication();
                }
            });
            this.menuItems = new MenuItem[] { mi, closeItem };

            if (this.minimized) {
                mi.setText("Maximize");
                mi.addSelectionListener(this.restoreWindowListener);
            } else {
                mi.setText("Minimize");
                mi.addSelectionListener(this.minimizeWindowListener);
            }
            this.menu.setVisible(true);
        }

        private void clearItems() {
            for (int i = 0; i < this.menuItems.length; i++) {
                MenuItem item = this.menuItems[i];
                item.removeSelectionListener(this.minimizeWindowListener);
                item.removeSelectionListener(this.restoreWindowListener);
                this.menuItems[i].dispose();
            }
        }

        public void handleEvent(Event event) {
            showMenu();
        }
    }
	
}
