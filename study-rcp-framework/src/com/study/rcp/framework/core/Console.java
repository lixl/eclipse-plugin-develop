package com.study.rcp.framework.core;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.console.MessageConsoleStream;

public class Console {
	private static MessageConsoleStream consoleStream;

	public static void info(final String _message) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				consoleStream = ConsoleFactory.getConsole().newMessageStream();
				ConsoleFactory.showConsole();
				consoleStream.println(new SimpleDateFormat("MM-dd hh:mm:ss")
						.format(new Date()) + "[INFO ]" + " " + _message);
			}
		});
	}

	public static void error(final String _message) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				consoleStream = ConsoleFactory.getConsole().newMessageStream();
				ConsoleFactory.showConsole();
				consoleStream.setColor(new Color(null, 255, 0, 0));
				consoleStream.println(new SimpleDateFormat("MM-dd hh:mm:ss")
						.format(new Date()) + "[ERROR]" + " " + _message);
			}
		});
	}
}
