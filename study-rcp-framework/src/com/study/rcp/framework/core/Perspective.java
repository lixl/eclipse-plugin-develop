package com.study.rcp.framework.core;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.console.IConsoleConstants;

import com.study.rcp.framework.treeview.NavigationView;
import com.study.rcp.framework.view.AutoGeneratorCodeView;

public class Perspective implements IPerspectiveFactory {

	/**
	 * The ID of the perspective as specified in the extension.
	 */
	public static final String ID = "rcpframework.perspective";

	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(false);
		
		layout.addStandaloneView(NavigationView.ID,  true, IPageLayout.LEFT, 0.15f, editorArea);
		IFolderLayout folder = layout.createFolder("messages", IPageLayout.TOP, 0.5f, editorArea);
		folder.addPlaceholder(AutoGeneratorCodeView.ID + ":*");
		folder.addView(AutoGeneratorCodeView.ID);
//		 layout.setFixed(true); //固定布局  
//		ConsoleViewPart cf  =   new  ConsoleViewPart();
//		cf.openConsole();
//		 layout.addView(IConsoleConstants.ID_CONSOLE_VIEW, IPageLayout.BOTTOM,0.70f, layout.getEditorArea());
		IFolderLayout folderLayout = layout.createFolder("folder", IPageLayout.BOTTOM,0.70f, editorArea);   
		folderLayout.addView(IConsoleConstants.ID_CONSOLE_VIEW);   
		layout.getViewLayout(NavigationView.ID).setCloseable(false);
	}
}
