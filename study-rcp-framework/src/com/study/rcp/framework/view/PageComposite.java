package com.study.rcp.framework.view;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;

/**
 * 前台分布控件
 * 
 * @author YinDang CreateTime:2011-7-22
 * 
 */
public class PageComposite {

	PageComposite() {
		super();
		// TODO Auto-generated constructor stub
	}

//	@Override
//	public Point getSize() {
//		return new Point(35, 800);
//	}
//
//	// 显示数据的表格
//	private SLKTable tabel;
//
//	private Label currentPageNo, SumPage, sumNum;
//	private Link initPage, prePage, nextPage, lastPage;
//	private Combo skipNum;
//	private Combo showNum;
//
//	private int sum = 0, shownum = 0, sumpage = 0, currentNo = 1;
//	private List list;
//
//	public List getList() {
//		return list;
//	}
//
//	public SLKTable getTabel() {
//		return tabel;
//	}
//
//	public void setTabel(SLKTable tabel) {
//		this.tabel = tabel;
//	}
//
//	public void setList(List list) {
//		this.list = list;
//		initData();
//	}
//
//	private void initData() {
//		if (null != list && list.size() > 0) {
//			// 设值
//			sum = list.size();
//			sumNum.setText(sum + "");
//
//			shownum = Integer
//					.valueOf("全部".equalsIgnoreCase(showNum.getText()) ? sum
//							+ "" : showNum.getText());
//			sumpage = sum / shownum;
//			if (sum % shownum > 0) {
//				sumpage++;
//			}
//			SumPage.setText(sumpage + "");
//
//			// 赋值到跳转框
//			skipNum.removeAll();
//			for (int i = 1; i <= sumpage; i++) {
//				skipNum.add(i + "");
//			}
//			skipNum.select(currentNo - 1);
//
//			currentPageNo.setText(currentNo + "");
//
//			// 初始跳转条数量
//			tabel.setInput(filterList(currentNo));
//
//			// 设置链接状态
//			if (sumpage == 1) {
//				initPage.setEnabled(false);
//				prePage.setEnabled(false);
//				nextPage.setEnabled(false);
//				lastPage.setEnabled(false);
//				return;
//			}
//			if (currentNo == 1) {
//				initPage.setEnabled(false);
//				prePage.setEnabled(false);
//				nextPage.setEnabled(true);
//				lastPage.setEnabled(true);
//			} else if (currentNo == sumpage) {
//				initPage.setEnabled(true);
//				prePage.setEnabled(true);
//				nextPage.setEnabled(false);
//				lastPage.setEnabled(false);
//			} else {
//				initPage.setEnabled(true);
//				prePage.setEnabled(true);
//				nextPage.setEnabled(true);
//				lastPage.setEnabled(true);
//			}
//		}
//	}
//
//	/**
//	 * Create the composite.
//	 * 
//	 * @param parent
//	 * @param style
//	 */
//	public PageComposite(Composite parent, List list, SLKTable tabel) {
//		super(parent, SWT.NONE);
//		this.list = list;
//		this.tabel = tabel;
//		this.setLayout(new FormLayout());
//		this.setBackground(RcpResourceManager.getColor(230, 235, 240));
//		this.setBackgroundMode(SWT.INHERIT_DEFAULT);
//		Label lblcp = new Label(this, SWT.NONE);
//		lblcp.setText("当前页：");
//		FormData fdlblcp = new FormData();
//		fdlblcp.top = new FormAttachment(0, 12);
//		fdlblcp.left = new FormAttachment(2);
//		lblcp.setLayoutData(fdlblcp);
//
//		currentPageNo = new Label(this, SWT.NONE);
//		currentPageNo.setText("0");
//		FormData fdcurrentPageNo = new FormData();
//		fdcurrentPageNo.left = new FormAttachment(lblcp, 3);
//		fdcurrentPageNo.top = new FormAttachment(0, 12);
//		fdcurrentPageNo.width = 30;
//		currentPageNo.setLayoutData(fdcurrentPageNo);
//
//		Label lblSumPage = new Label(this, SWT.NONE);
//		lblSumPage.setText("总页数:");
//		FormData fdlblSumPage = new FormData();
//		fdlblSumPage.top = new FormAttachment(0, 12);
//		fdlblSumPage.left = new FormAttachment(currentPageNo, 12);
//		lblSumPage.setLayoutData(fdlblSumPage);
//
//		SumPage = new Label(this, SWT.NONE);
//		SumPage.setText("0");
//		FormData fdSumPage = new FormData();
//		fdSumPage.left = new FormAttachment(lblSumPage, 3);
//		fdSumPage.top = new FormAttachment(0, 12);
//		fdSumPage.width = 30;
//		SumPage.setLayoutData(fdSumPage);
//
//		Label lblSumNum = new Label(this, SWT.NONE);
//		lblSumNum.setText("总数量(条):");
//		FormData fdlblSumNum = new FormData();
//		fdlblSumNum.top = new FormAttachment(0, 12);
//		fdlblSumNum.left = new FormAttachment(SumPage, 12);
//		lblSumNum.setLayoutData(fdlblSumNum);
//
//		sumNum = new Label(this, SWT.NONE);
//		sumNum.setText("0");
//		FormData fdsumNum = new FormData();
//		fdsumNum.left = new FormAttachment(lblSumNum, 3);
//		fdsumNum.top = new FormAttachment(0, 12);
//		fdsumNum.width = 30;
//		sumNum.setLayoutData(fdsumNum);
//
//		initPage = new Link(this, SWT.NONE);
//		initPage.setText("<a>首页</a>");
//		FormData fdinitPage = new FormData();
//		fdinitPage.left = new FormAttachment(sumNum, 27);
//		fdinitPage.top = new FormAttachment(0, 12);
//		initPage.setLayoutData(fdinitPage);
//		initPage.setEnabled(false);
//
//		prePage = new Link(this, SWT.NONE);
//		prePage.setText("<a>上一页</a>");
//		FormData fdprePage = new FormData();
//		fdprePage.left = new FormAttachment(initPage, 12);
//		fdprePage.top = new FormAttachment(0, 12);
//		prePage.setLayoutData(fdprePage);
//		prePage.setEnabled(false);
//
//		nextPage = new Link(this, SWT.NONE);
//		nextPage.setText("<a>下一页</a>");
//		FormData fdnextPage = new FormData();
//		fdnextPage.left = new FormAttachment(prePage, 12);
//		fdnextPage.top = new FormAttachment(0, 12);
//		nextPage.setLayoutData(fdnextPage);
//		nextPage.setEnabled(false);
//
//		lastPage = new Link(this, SWT.NONE);
//		lastPage.setText("<a>末页</a>");
//		FormData fdlastPage = new FormData();
//		fdlastPage.left = new FormAttachment(nextPage, 12);
//		fdlastPage.top = new FormAttachment(0, 12);
//		lastPage.setLayoutData(fdlastPage);
//		lastPage.setEnabled(false);
//
//		Label skipPage = new Label(this, SWT.NONE);
//		skipPage.setText("跳转指定页");
//		FormData fdskipPage = new FormData();
//		fdskipPage.left = new FormAttachment(lastPage, 20);
//		fdskipPage.top = new FormAttachment(0, 12);
//		skipPage.setLayoutData(fdskipPage);
//
//		skipNum = new Combo(this, SWT.BORDER | SWT.READ_ONLY);
//		FormData fdskipNum = new FormData();
//		fdskipNum.left = new FormAttachment(skipPage, 7);
//		fdskipNum.top = new FormAttachment(0, 9);
//		fdskipNum.width = 40;
//		skipNum.setLayoutData(fdskipNum);
//
//		Label showPage = new Label(this, SWT.NONE);
//		showPage.setText("每页显示数量");
//		FormData fdsshowPage = new FormData();
//		fdsshowPage.left = new FormAttachment(skipNum, 17);
//		fdsshowPage.top = new FormAttachment(0, 12);
//		showPage.setLayoutData(fdsshowPage);
//
//		showNum = new Combo(this, SWT.BORDER | SWT.READ_ONLY);
//		FormData fdshowNum = new FormData();
//		fdshowNum.left = new FormAttachment(showPage, 7);
//		fdshowNum.top = new FormAttachment(0, 9);
//		showNum.setLayoutData(fdshowNum);
//		showNum.add("5");
//		showNum.add("15");
//		showNum.add("25");
//		showNum.add("50");
//		showNum.add("100");
//		showNum.add("全部");
//		showNum.select(1);
//
//		Label dw = new Label(this, SWT.NONE);
//		dw.setText("条");
//		FormData fddw = new FormData();
//		fddw.left = new FormAttachment(showNum, 3);
//		fddw.top = new FormAttachment(0, 12);
//		dw.setLayoutData(fddw);
//		// 设置字体
//		setFont();
//		// 初始化数据
//		initData();
//		// 添加监听
//		addListener();
//	}
//
//	private void setFont() {
//		Control[] lbls = this.getChildren();
//		for (final Control control : lbls) {
//			if (control instanceof Label) {
//				control.setFont(ZhcxFont.f11);
//			}
//			if (control instanceof Link) {
//				control.setFont(ZhcxFont.f11);
//				control.addMouseTrackListener(new MouseTrackListener() {
//
//					public void mouseHover(MouseEvent e) {
//
//					}
//
//					public void mouseExit(MouseEvent e) {
//						control.setBackground(RcpResourceManager.getColor(230,
//								235, 240));
//					}
//
//					public void mouseEnter(MouseEvent e) {
//						control.setBackground(RcpResourceManager.getColor(87,
//								103, 129));
//					}
//				});
//			}
//		}
//	}
//
//	/**
//	 * 根据用户选择指定页，从全局集合里面设置数据
//	 * 
//	 * @param pageNo
//	 * @return
//	 */
//	public List filterList(int pageNo) {
//		if (null == list || list.size() <= 0) {
//			return null;
//		}
//		List tempList = new ArrayList();
//		for (int i = shownum * pageNo - shownum; i < shownum * pageNo; i++) {
//			if (i == list.size()) {
//				return tempList;
//			}
//			tempList.add(list.get(i));
//		}
//		return tempList;
//	}
//
//	private void addListener() {
//		initPage.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				currentNo = 1;
//				initData();
//			}
//		});
//		prePage.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				// 正常情况下
//				currentNo = currentNo - 1;
//				initData();
//			}
//		});
//		nextPage.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				// 正常情况下
//				currentNo = currentNo + 1;
//				initData();
//			}
//		});
//		lastPage.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				currentNo = sumpage;
//				initData();
//			}
//		});
//		skipNum.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				currentNo = Integer.valueOf(skipNum.getText());
//				if (currentNo > sumpage) {
//					return;
//				}
//				initData();
//			}
//		});
//		showNum.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				currentNo = 1;
//				initData();
//			}
//		});
//	}
//
//	public void setLayout(FormData formData) {
//		this.setLayoutData(formData);
//	}
//
//	@Override
//	protected void checkSubclass() {
//	}
}
