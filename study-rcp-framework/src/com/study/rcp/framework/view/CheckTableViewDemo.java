package com.study.rcp.framework.view;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class CheckTableViewDemo {

	CheckboxTableViewer checkTableView;

	public CheckTableViewDemo(Shell parent) {
		
		createToolBar(parent);
		
		createTable(parent);
		
		checkTableView.setInput(getInitParams());
	}
	/**
	 * 创建工具栏
	 * @param parent
	 */
	private void createToolBar(Composite parent) {
		//添加工具栏
		ToolBar toolBar = new ToolBar(parent.getShell(), SWT.FLAT | SWT.WRAP | SWT.RIGHT);
		//添加工具项
		ToolItem allCheck = new ToolItem(toolBar, SWT.NONE);
		allCheck.setText("全选");
		
		new ToolItem(toolBar, SWT.SEPARATOR);
		
		//添加工具项
		ToolItem rickCheck = new ToolItem(toolBar, SWT.NONE);
		rickCheck.setText("反选");
		
		new ToolItem(toolBar, SWT.SEPARATOR);
		
		//添加工具项
		ToolItem showCheck = new ToolItem(toolBar, SWT.NONE);
		showCheck.setText("查看选则");
		
		
		//添加监听
		allCheck.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				Display.getDefault().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						//全选
						checkTableView.setAllChecked(true);
						
					}
				});
				
				
			}
		});
		
		rickCheck.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				
				Display.getDefault().asyncExec(new Runnable() {
					
					@SuppressWarnings("unchecked")
					@Override
					public void run() {
						List<Person> list =  (List<Person>) checkTableView.getInput();
						
						//进行反选
						for(Person p : list) {
							checkTableView.setChecked(p, !checkTableView.getChecked(p));
						}
						
					}
				});
			
			}
		});
		
		//查看选则
		showCheck.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				
				//我们起一个dialog来显示一下
				//取出选中的元素
				Object[] checks = checkTableView.getCheckedElements();
				
				//如果选则为空 提示下
				if(checks == null || checks.length == 0) {
					MessageDialog.openInformation(Display.getDefault().getActiveShell(), "提示", "选则为空!");
					return;
				}
					
				
				//转换为字符串弹出dialog
				StringBuffer sb = new StringBuffer();
				for(Object obj : checks) {
					if(obj instanceof Person) {
						sb.append(((Person)obj).getId() + " , ");
					}
				}
				
				MessageDialog.openInformation(Display.getDefault().getActiveShell(), "选中元素的ID", sb.toString());
				
			}
		});
		
	}
	
	/**
	 * 创建表
	 * @param parent
	 */
	private void createTable(Composite parent) {
		//先创建表 注意style中要包含 SWT.CHECK
		Table table = new Table(parent, SWT.CHECK|SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER|SWT.V_SCROLL|SWT.H_SCROLL);
		table.setLayoutData(new GridData(GridData.FILL_BOTH));
		//设置表头
		String[] tableCols = {"ID" , "姓名" , "性别" , "电话"};
		
		for(String colStr : tableCols) {
			TableColumn tc = new TableColumn(table, SWT.LEFT); //靠左
			tc.setText(colStr);
			tc.setWidth(70);
		}
		//设置line 和header显示
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		
		//创建tableview
		checkTableView = new CheckboxTableViewer(table);
		checkTableView.setLabelProvider(new PersonLabelProvider());
		checkTableView.setContentProvider(new PersonContentProvider());
		
	}
	
	/**
	 * 初始化一些参数进来
	 * @return
	 */
	private List<Person> getInitParams() {
		List<Person> persons = new ArrayList<Person>();
		
		for(int i = 0 ; i < 10 ; i++) {
			Person p = new Person();
			p.setId("00" + i);
			p.setName("小刘" + i);
			p.setSex(Math.random() > 0.5 ? "男" : "女");
			p.setTel((int)(Math.random() * 1000000000) + "");
			
			persons.add(p);
		}
		
		
		
		return persons;
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display, SWT.MAX | SWT.CLOSE);
		shell.setLayout(new GridLayout(1 , false));
		shell.setText("CheckTableViewDemo");
		
		new CheckTableViewDemo(shell);
		

		shell.open();
		while (!shell.isDisposed())
			if (display.readAndDispatch())
				display.sleep();
		display.dispose();

	}

	/**
	 * 标签管理
	 * @author lyf
	 *
	 */
	private class PersonLabelProvider extends LabelProvider implements
			ITableLabelProvider {

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			switch (columnIndex) {
			case 0:
				return ((Person)element).getId();
			case 1:
				return ((Person)element).getName();
			case 2:
				return ((Person)element).getSex();
			case 3:
				return ((Person)element).getTel();
				

			default:
				return "";
			}
			
		}

	}
	
	/**
	 * 内容管理
	 * 
	 * @author lyf
	 * 
	 */
	private class PersonContentProvider implements
			IStructuredContentProvider {

		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			// TODO Auto-generated method stub
			
		}

		@SuppressWarnings("rawtypes")
		@Override
		public Object[] getElements(Object inputElement) {
			if(inputElement instanceof List) {
				return ((List)inputElement).toArray();
			}
			return new Object[0];
		}
		
	}

}
