package com.demo.itextdemo;

import java.io.FileOutputStream;
import java.io.OutputStream;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

/**
 * Hello world!
 * http://developers.itextpdf.com/examples-itext7
 */
public class App {
	public static void main(String[] args) throws Exception {
		OutputStream dest = new FileOutputStream("e:/itext7demo.pdf");
		PdfWriter writer = new PdfWriter(dest);
		PdfDocument pdf = new PdfDocument(writer);
		Document document = new Document(pdf);
		document.add(new Paragraph("Hello World!"));
		document.close();
		writer.close();
		dest.close();
	}
}
