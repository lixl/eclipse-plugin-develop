package demo.core.about;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.internal.dialogs.AboutDialog;

@SuppressWarnings("restriction")
public class AboutAction extends Action implements IWorkbenchAction {  
	  
    private IWorkbenchWindow workbenchWindow;  
      
    public AboutAction(IWorkbenchWindow window){  
        if(window==null){  
            throw new IllegalArgumentException();  
        }  
        this.workbenchWindow=window;  
    }  
      
    public void run(){  
        if(workbenchWindow !=null){  
            AboutDialog ad=new AboutDialog(workbenchWindow.getShell());  
            ad.open();  
        }  
    }  
      
    @Override  
    public void dispose() {  
          
    }  
  
}  
