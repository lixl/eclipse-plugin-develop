package demo.core.about;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class AboutDialog extends Dialog {

	protected Shell shell;

	private int result;

	public AboutDialog(Shell parent) {
		this(parent, SWT.NONE);
	}

	public AboutDialog(Shell parent, int style) {
		super(parent, style);
	}

	public int open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		return result;
	}

	protected void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		shell.setSize(400, 150);
		shell.setText("关于 ");

		Label label2 = new Label(shell, SWT.NONE);
		label2.setText("自定义菜单");

		label2.setBounds(70, 50, 300, 20);
	}

}
