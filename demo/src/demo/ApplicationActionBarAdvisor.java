package demo;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

import demo.core.about.AboutAction;
import demo.core.openfile.OpenFileAction;

public class ApplicationActionBarAdvisor extends ActionBarAdvisor {

	private IWorkbenchAction exitAction;

	private IWorkbenchAction openFileAction;

	private IWorkbenchAction iSaveAction;
	
	private IWorkbenchAction aboutAction;

	public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
		super(configurer);
	}

	/**
	 * 定义菜单按钮事件
	 */
	protected void makeActions(IWorkbenchWindow window) {
		exitAction = ActionFactory.QUIT.create(window);
		register(exitAction);
		iSaveAction = ActionFactory.SAVE.create(window);
		register(iSaveAction);
		openFileAction = new OpenFileAction();
		openFileAction.setId("test.openfile");
		openFileAction.setText("Open File...");
		register(openFileAction);
		
		aboutAction = new AboutAction(window);  
        aboutAction.setId("test.about");  
        aboutAction.setText("关于");  
        register(aboutAction);  
	}

	/**
	 * 定义菜单按钮分布
	 */
	protected void fillMenuBar(IMenuManager menuBar) {
		MenuManager fileMenu = new MenuManager("&File",
				IWorkbenchActionConstants.M_FILE);
		menuBar.add(fileMenu);

		fileMenu.add(openFileAction);
		fileMenu.add(iSaveAction);
		fileMenu.add(new Separator());
		fileMenu.add(exitAction);

		MenuManager aboutMenu = new MenuManager("About");  
		aboutMenu.add(aboutAction);  
        menuBar.add(aboutMenu);  
	}

}
